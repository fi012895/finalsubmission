// we need the include files io and stdlib and string and stdbool and time
// we need to define a constant for the number of rooms
// we need to define a constant for the maximum number of connections per room


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

// we want to write a text based game that is a little bit like a choose your own adventure book
// each turn we will present the player with a choice of actions
// the player will choose an action and we will carry it out
// the player can look at the room, or move to another room


// we will need to keep track of the current room
// we need a way to represent the rooms
// we need a way to represent the connections between the rooms
// let's have a datastructure that represents the rooms
// room has connections and connections have rooms so we need to define connection first but we can't because room needs it
// we can use a forward declaration to tell the compiler that connection is a struct that will be defined later

#define MAX_ITEMS 10
#define MAX_NAME_LEN 200
#define MAX_DESC_LEN 200

typedef struct item {
    char name[MAX_NAME_LEN];
    char description[MAX_DESC_LEN];
    bool collected;
} item;


typedef struct connection connection;

typedef struct room 
{
  char* name;
  char* description;
  connection* connections;
  int numConnections;
  item items[MAX_ITEMS];
  int itemCount;
} room;

// we need a way to represent the connections between the rooms
// let's have a datastructure that represents the connections
typedef struct connection 
{
  room* room1;
  room* room2;
} connection;

// we need a way to represent the player
// let's have a datastructure that represents the player
typedef struct player 
{
  item inventory[MAX_ITEMS];
  int inventoryCount;
  room* currentRoom;
} player;

// we need a way to represent the game
// let's have a datastructure that represents the game
typedef struct game 
{
  room* rooms;
  int numRooms;
  player* player;
  item* availableItems; // Change availableItems to a pointer
  int availableItemCount;
} game;

// Function to load items from items.csv file
int loadItems(item items[]) 
{
    printf("debug - about to load items from file\n");
    // Implement loading items from items.csv here
    FILE* file = fopen("items.csv", "r");
    if (file == NULL) {
        printf("Error opening items.csv file.\n");
        return 0;
    }

    int count = 0;
    char line[256];
    while (fgets(line, sizeof(line), file)) {
        if (sscanf(line, "\"%49[^\"]\",\"%49[^\"]\"", items[count].name, items[count].description) == 2) {
            items[count].collected = false;
            count++;
            if (count >= MAX_ITEMS) {
                break;
            }
        }
    }

    fclose(file);
    printf("debug - items loaded from file\n");
    return count;
}





// Function to place items in rooms at the start of the game
void placeItemsInRooms(game* g)
{
    printf("debug - about to place items in rooms\n");

    // Seed for randomness
    srand(time(NULL));

    // Shuffle the availableItems array to randomize the order
    for (int i = g->availableItemCount - 1; i > 0; --i) {
        int j = rand() % (i + 1);

        item temp = g->availableItems[i];
        g->availableItems[i] = g->availableItems[j];
        g->availableItems[j] = temp;
    }

    // Allocate a boolean array to keep track of used items
    bool usedItems[MAX_ITEMS];
    memset(usedItems, false, sizeof(usedItems)); // Initialize all to false

    for (int i = 0; i < g->numRooms; ++i) 
    {
        printf("Debug: Placing item in room %d\n", i);

        room* currentRoom = &(g->rooms[i]);

        // Find the next available unique item for this room
        int itemIndex = -1;
        for (int j = 0; j < g->availableItemCount; ++j) 
        {
            if (!usedItems[j]) {
                itemIndex = j;
                usedItems[j] = true; // Mark this item as used
                break;
            }
        }

        // If there's an available unique item, assign it to the room
        if (itemIndex != -1) 
        {
            currentRoom->items[currentRoom->itemCount] = g->availableItems[itemIndex];
            currentRoom->itemCount++;
        }
        else 
        {
            printf("Error: Not enough unique items for each room.\n");
            break;
        }
        
    }

    printf("debug - finished placing items in rooms\n");
}


// Function to collect an item and add it to the player's inventory
void collectItem(player* p, item* newItem) 
{
    // Implement collecting item here
    printf("debug - about to collect item\n");
    if (p->inventoryCount < MAX_ITEMS) {
        // Add the item to the player's inventory
        p->inventory[p->inventoryCount] = *newItem;
        p->inventoryCount++;
        printf("You collected: %s\n", newItem->name);
        newItem->collected = true; // Mark the item as collected in the room
        printf("debug - collected item\n");
    }
    else {
        printf("Your inventory is full!\n");
    }

}

void checkRoomForItems(player* p, room* currentRoom) 
{
    printf("debug - about to check rooms for items\n");
    // Implement checking room for items here
    if (currentRoom->itemCount > 0) {
        printf("You found some items in this room!\n");

        for (int i = 0; i < currentRoom->itemCount; ++i) {
            item* currentItem = &(currentRoom->items[i]);

            if (!(currentItem->collected)) {
                printf("You found: %s - %s\n", currentItem->name, currentItem->description);
                printf("Do you want to collect this item? (Y/N): ");
                char response;
                scanf(" %c", &response);

                if (response == 'Y' || response == 'y') {
                    collectItem(p, currentItem);
                }
            }
        }
    }
    else {
        printf("There are no items in this room.\n");
    }
    printf("debug - checked rooms for items\n");
}

void viewInventory(game* g) 
{
    printf("Inventory:\n");
    for (int i = 0; i < g->player->inventoryCount; ++i) 
    {
        printf("%s - %s\n", g->player->inventory[i].name, g->player->inventory[i].description);
    }
}


// let's have a function to print a menu and get user choice, return the choice
int getMenuChoice() 
{
  int choice;
  printf("What would you like to do?\n");
  printf("1. Look around\n");
  printf("2. Move to another room\n");
  printf("3. View Inventory\n");
  printf("4. Quit\n");
  scanf("%d", &choice);
  // we need to check that the choice is valid
  while (choice < 1 || choice > 4) 
  {
    printf("Invalid choice, please try again\n");
    scanf("%d", &choice);
  }
  return choice;
}

// let's have a function to print the room description
void printRoomDescription(room* room) 
{
  printf("You are in the %s.\n", room->name);
  printf("%s\n", room->description);
}

// a function to get user choice of room to move to
int getRoomChoice(room* currentRoom) 
{
  int choice;
  printf("Which room would you like to move to?\n");
  for (int i = 0; i < currentRoom->numConnections; i++) 
  {
    printf("%d. %s\n", i+1, currentRoom->connections[i].room2->name);
  }
  scanf("%d", &choice);
  // we need to check that the choice is valid
  while (choice < 1 || choice > currentRoom->numConnections) 
  {
    printf("Invalid choice, please try again\n");
    scanf("%d", &choice);
  }
  return choice;
}

// a function to move the player to another room, and describe it to the user
void movePlayer(player* player, int choice) 
{
  player->currentRoom = player->currentRoom->connections[choice-1].room2;
  printRoomDescription(player->currentRoom);
}


// a function to load the rooms from a file
// the file is called rooms.csv, and has a room name and room description on each line
// the function returns an array of rooms
room* loadRooms() 
{
  // open the file
  FILE* file = fopen("rooms.csv", "r");
  // we need to check that the file opened successfully
  if (file == NULL) 
  {
    printf("Error opening file\n");
    exit(1);
  }
  // we need to count the number of lines in the file
  int numLines = 0;
  char line[500];
  while (fgets(line, 500, file) != NULL) 
  {
    numLines++;
  }
  // we need to rewind the file
  rewind(file);
  // we need to allocate memory for the rooms
  room* rooms = malloc(sizeof(room) * numLines);
  // we need to read the rooms from the file
  
  for (int i = 0; i < numLines; i++) 
  {
    // we need to read the line
    fgets(line, 500, file);
    // we need to remove the newline character
    line[strlen(line)-1] = '\0';
    // we need to split the line into the name and description
    // the strings are in quotation marks, and the two quoted strings are separated by a comma
    // we need to split the line on ", " (the four characters)
    //everything between the first and second " is the name
    //everything between the third and fourth " is the description
    // we need to find the first "
    char* name = strtok(line, "\"");
    // we need to find the second " and record where it is so we can null terminate the string
    char* endofname=strtok(NULL, "\"");
    
    
    // we need to find the third "
    char* description = strtok(NULL, "\"");
    // we need to find the fourth "
    char* endofdesc=strtok(NULL, "\0");
    //we need to be sure that name and description are null terminated strings
    name[endofname-name]='\0';
    //description[endofdesc-description]='\0';
    // we need to create a room
    room room;
    //we need to copy the string into the room name
    room.name = malloc(sizeof(char) * strlen(name) + 1);
    strcpy(room.name, name);
    //we need to copy the string into the room description
    room.description = malloc(sizeof(char) * strlen(description) + 1);
    strcpy(room.description, description);
    room.connections = NULL;
    room.numConnections = 0;
    // we need to add the room to the array
    rooms[i] = room;
  }
  // we need to close the file
  fclose(file);

  FILE* connectionsFile = fopen("connections.csv", "r");
  if (connectionsFile == NULL)
  {
    printf("Error opening connections file\n");
    exit(1);
  }

  int roomConnections[10][3];

  for (int i=0; i < 10; i++)
  {
    if (fscanf(connectionsFile, "%d,%d,%d\n", &roomConnections[i][0], 
        &roomConnections[i][1], &roomConnections[i][2]) != 3)
    {
      printf("Error reading connections from file\n");
      exit(1);
    }
  }

  fclose(connectionsFile);

  for (int i=0; i < numLines; i++)
  {
    for(int j=0; j < 3; j++)
    {
      int connectedRoomIndex = roomConnections[i][j];
      if (connectedRoomIndex >= 0 && connectedRoomIndex < numLines 
          && i != connectedRoomIndex)
      {
        connection conn1, conn2;
        conn1.room1 = &rooms[i];
        conn1.room2 = &rooms[connectedRoomIndex];
        conn2.room1 = &rooms[connectedRoomIndex];
        conn2.room2 = &rooms[i];

        rooms[i].connections = realloc(rooms[i].connections, sizeof(connection)
                                        * (rooms[i].numConnections +1));
        rooms[i].connections[rooms[i].numConnections] = conn1;
        rooms[i].numConnections++;

        rooms[connectedRoomIndex].connections = realloc(rooms[connectedRoomIndex].connections,
                             sizeof(connection) * (rooms[connectedRoomIndex].numConnections +1)); 
        rooms[connectedRoomIndex].connections[rooms[connectedRoomIndex].numConnections] = conn2;
        rooms[connectedRoomIndex].numConnections++;
      }
    }
  }
  return rooms;
}

// let's have a function to create a player
player* createPlayer(room* currentRoom) 
{
  // we need to allocate memory for the player
  player* player = malloc(sizeof(player));
  // we need to set the current room
  player->currentRoom = currentRoom;
  // we need to return the player
  return player;
}

// let's have a function to create a game
game* createGame() 
{
  // we need to allocate memory for the game
  game* game = malloc(sizeof(game));

  if (game == NULL) {
      // Handle allocation failure for game structure
      printf("Memory allocation failed for game.\n");
      // Handle error or exit gracefully
  }

  // we need to load the rooms
  printf("debug - about to load rooms\n");
  game->rooms = loadRooms();
  printf("debug - rooms loaded\n");
  // we need to set the number of rooms
  game->numRooms = 10;
  // we need to create the player
  game->player = createPlayer(&game->rooms[0]);
  // we need to return the game

  // Load items
  item loadedItems[MAX_ITEMS];
  int loadedItemCount = loadItems(loadedItems);

  // Allocate memory for available items
  game->availableItems = malloc(sizeof(item) * loadedItemCount);
  memcpy(game->availableItems, loadedItems, sizeof(item) * loadedItemCount);
  game->availableItemCount = loadedItemCount;


  // Place items in rooms
  placeItemsInRooms(game);
  

  return game;
}

// let's have a function to play the game which is the main function
void playGame() 
{
  // we need to create the game
  printf("Welcome to the game\n");
  game* game = createGame();
  // we need to print the room description
  printRoomDescription(game->player->currentRoom);
  // we need to loop until the user quits
  bool quit = false;
  while (!quit) 
  {
    // we need to print the menu and get the user choice
    int choice = getMenuChoice();
    // we need to carry out the user choice
    if (choice == 1) 
    {
      // we need to print the room description
      printRoomDescription(game->player->currentRoom);
    } 
    if (choice == 2) 
    {
        int choice = getRoomChoice(game->player->currentRoom);
        movePlayer(game->player, choice);
        checkRoomForItems(game->player, game->player->currentRoom); // Call this function when entering a room
    }
    else if (choice == 3)
    {
        viewInventory(game); 
    }
    else if (choice == 4) 
    {
      // we need to quit
      quit = true;

    }
  }
}

// let's have a main function to call the playGame function
int main() 
{
    playGame();
  return 0;
}
